package online.octoapps.radiousa.presentation.stations;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import online.octoapps.radiousa.domain.radioplayer.RadioPlayerInteractor;
import online.octoapps.radiousa.domain.radioplayer.event.ErrorEvent;
import online.octoapps.radiousa.domain.radioplayer.event.LoadingEvent;
import online.octoapps.radiousa.domain.radioplayer.event.PlayingStartedEvent;
import online.octoapps.radiousa.domain.radioplayer.event.PlayingStoppedEvent;
import online.octoapps.radiousa.domain.stations.event.StationsLoadingFailureEvent;
import online.octoapps.radiousa.domain.stations.event.StationsLoadingSuccessfulEvent;
import online.octoapps.radiousa.domain.stations.event.StationsSearchingFailureEvent;
import online.octoapps.radiousa.domain.stations.event.StationsSearchingSuccessfulEvent;
import online.octoapps.radiousa.domain.stations.StationsInteractor;
import online.octoapps.radiousa.util.LogUtil;

public class StationsPresenter implements StationsContract.Presenter {
    private StationsContract.View mView;
    private StationsInteractor mStationsInteractor;
    private RadioPlayerInteractor mRadioPlayerInteractor;

    private int mPage;
    private boolean mIsSearch;
    private String mSearchQuery;
    private boolean mIsLoadFavorites;

    public StationsPresenter(StationsContract.View view) {
        mView = view;
    }

    @Override
    public void attachStationsInteractor(StationsInteractor interactor) {
        mStationsInteractor = interactor;
    }

    @Override
    public void attachRadioPlayerInteractor(RadioPlayerInteractor interactor) {
        mRadioPlayerInteractor = interactor;
    }

    @Override
    public void onCreate() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        mView = null;
        mStationsInteractor = null;
        mRadioPlayerInteractor = null;
    }

    @Override
    public void onLoadMoreStations() {
        if (mStationsInteractor == null) return;
        if (mIsSearch) mStationsInteractor.searchStations(mSearchQuery);
        else if(mIsLoadFavorites) mStationsInteractor.loadFavoriteStations(mPage);
        else mStationsInteractor.loadAllStations(mPage);
    }

    @Override
    public void onAddToFavoritesButtonClick(long stationId) {
        if (mStationsInteractor == null) return;
        mStationsInteractor.addStationToFavorites(stationId);
    }

    @Override
    public void onRemoveFromFavoritesButtonClick(long stationId) {
        if (mStationsInteractor == null) return;
        mStationsInteractor.removeStationFromFavorites(stationId);
    }

    @Override
    public void onStationsItemClick(long stationId) {
        if (mRadioPlayerInteractor == null) return;
        mRadioPlayerInteractor.togglePlayingState(stationId);
    }

    @Override
    public void onSearchQueryTextChange(String newText) {
        if (mView == null || newText.length() > 0 && newText.length() < 3) return;
        else if (newText.isEmpty()) {
            mIsSearch = false;
            mPage = 0;
        } else {
            mIsSearch = true;
            mSearchQuery = newText;
        }

        mView.setHasLoadedAllItems(false);
        mView.setHasMoreDataToLoad(true);
        mView.clearStations();
    }

    @Override
    public void onShowAllMenuItemClick() {
        if (mStationsInteractor == null || mView == null) return;

        mView.setShowAllMenuItemVisible(false);
        mView.setShowFavoritesMenuItemVisible(true);
        mView.setShowFavoritesMenuItemEnabled(false);

        mView.setHasLoadedAllItems(false);
        mView.setHasMoreDataToLoad(true);

        mPage = 0;
        mIsLoadFavorites = false;
        mView.clearStations();
    }

    @Override
    public void onShowFavoritesMenuItemClick() {
        if (mStationsInteractor == null || mView == null) return;

        mView.setShowFavoritesMenuItemVisible(false);
        mView.setShowAllMenuItemVisible(true);
        mView.setShowAllMenuItemEnabled(false);

        mView.setHasLoadedAllItems(false);
        mView.setHasMoreDataToLoad(true);

        mPage = 0;
        mIsLoadFavorites = true;
        mView.clearStations();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(StationsLoadingSuccessfulEvent event) {
        if (mView == null) return;

        if(mIsLoadFavorites) mView.setShowAllMenuItemEnabled(true);
        else mView.setShowFavoritesMenuItemEnabled(true);

        if(mPage == 0) {
            mView.setHasLoadedAllItems(false);
            mView.clearStations();
        }
        mView.addStations(event.getStationViewModels());
        mPage++;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(StationsLoadingFailureEvent event) {
        if (mView == null) return;

        if(mIsLoadFavorites) mView.setShowAllMenuItemEnabled(true);
        else mView.setShowFavoritesMenuItemEnabled(true);

        mView.setHasMoreDataToLoad(false);
        mView.setHasLoadedAllItems(true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(StationsSearchingSuccessfulEvent event) {
        if (!mIsSearch || mView == null) return;
        mView.setHasLoadedAllItems(true);
        mView.setHasMoreDataToLoad(false);
        mView.clearStations();
        mView.addStations(event.getStationViewModels());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(StationsSearchingFailureEvent event) {
        // TODO: 11.02.17
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ErrorEvent event) {
        LogUtil.i(this, "ErrorEvent");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LoadingEvent event) {
        LogUtil.i(this, "LoadingEvent");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PlayingStartedEvent event) {
        LogUtil.i(this, "PlayingStartedEvent");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PlayingStoppedEvent event) {
        LogUtil.i(this, "PlayingStoppedEvent");
    }

}
