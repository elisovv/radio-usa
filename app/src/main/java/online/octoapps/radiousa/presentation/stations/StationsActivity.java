package online.octoapps.radiousa.presentation.stations;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.paginate.Paginate;

import java.util.List;

import online.octoapps.radiousa.R;
import online.octoapps.radiousa.domain.radioplayer.RadioPlayerInteractorImpl;
import online.octoapps.radiousa.domain.stations.StationsInteractorImpl;
import online.octoapps.radiousa.data.StationsRepository;
import online.octoapps.radiousa.data.StationsRepositoryImpl;
import online.octoapps.radiousa.presentation.stations.adapter.StationsAdapter;
import online.octoapps.radiousa.presentation.stations.viewmodel.StationViewModel;

public class StationsActivity extends AppCompatActivity implements StationsContract.View, Paginate.Callbacks, StationsAdapter.Callbacks {
    private StationsContract.Presenter mPresenter;

    private MenuItem mShowAllMenuItem;
    private MenuItem mShowFavoritesMenuItem;

    private Paginate mPaginate;
    private boolean mIsLoading;
    private boolean mHasLoadedAllItems;

    private RecyclerView mStationsRecycler;
    private StationsAdapter mStationsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stations);
        initActionBar();
        initStationsAdapter();
        initStationsRecycler();
        initPresenter();
        initPaginate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.stations_menu, menu);
        mShowAllMenuItem = menu.findItem(R.id.menu_show_all);
        mShowFavoritesMenuItem = menu.findItem(R.id.menu_show_favorites);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search)
                .getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mPresenter.onSearchQueryTextChange(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_show_all:
                mPresenter.onShowAllMenuItemClick();
                break;
            case R.id.menu_show_favorites:
                mPresenter.onShowFavoritesMenuItemClick();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void addStations(List<StationViewModel> items) {
        mStationsAdapter.addItems(items);
        mIsLoading = false;
    }

    @Override
    public void clearStations() {
        mStationsAdapter.clearItems();
    }

    @Override
    public void setHasMoreDataToLoad(boolean b) {
        mPaginate.setHasMoreDataToLoad(b);
        mIsLoading = false;
    }

    @Override
    public void setHasLoadedAllItems(boolean b) {
        if (b) mIsLoading = false;
        mHasLoadedAllItems = b;
    }

    @Override
    public void setShowAllMenuItemVisible(boolean b) {
        mShowAllMenuItem.setVisible(b);
    }

    @Override
    public void setShowFavoritesMenuItemVisible(boolean b) {
        mShowFavoritesMenuItem.setVisible(b);
    }

    @Override
    public void setShowAllMenuItemEnabled(boolean b) {
        mShowAllMenuItem.setEnabled(b);
    }

    @Override
    public void setShowFavoritesMenuItemEnabled(boolean b) {
        mShowFavoritesMenuItem.setEnabled(b);
    }

    @Override
    public void onLoadMore() {
        mIsLoading = true;
        mPresenter.onLoadMoreStations();
    }

    @Override
    public boolean isLoading() {
        return mIsLoading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return mHasLoadedAllItems;
    }

    @Override
    public void onItemClick(long stationId) {
        mPresenter.onStationsItemClick(stationId);
    }

    @Override
    public void onAddToFavoritesButtonClick(long stationId) {
        mPresenter.onAddToFavoritesButtonClick(stationId);
    }

    @Override
    public void onRemoveFromFavoritesButtonClick(long stationId) {
        mPresenter.onRemoveFromFavoritesButtonClick(stationId);
    }

    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void initStationsAdapter() {
        mStationsAdapter = new StationsAdapter();
        mStationsAdapter.setCallbacks(this);
    }

    private void initStationsRecycler() {
        mStationsRecycler = (RecyclerView) findViewById(R.id.recycler_global_stations);
        mStationsRecycler.setLayoutManager(new LinearLayoutManager(this));
        mStationsRecycler.setAdapter(mStationsAdapter);
    }

    private void initPaginate() {
        mPaginate = Paginate.with(mStationsRecycler, this)
                .setLoadingTriggerThreshold(0)
                .addLoadingListItem(true)
                .build();
    }

    private void initPresenter() {
        mPresenter = new StationsPresenter(this);
        StationsRepository stationsRepository = new StationsRepositoryImpl(this);
        mPresenter.attachStationsInteractor(new StationsInteractorImpl(stationsRepository));
        mPresenter.attachRadioPlayerInteractor(new RadioPlayerInteractorImpl(this));
        mPresenter.onCreate();
    }
}
