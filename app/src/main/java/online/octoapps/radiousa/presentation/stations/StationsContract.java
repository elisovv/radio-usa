package online.octoapps.radiousa.presentation.stations;

import java.util.List;

import online.octoapps.radiousa.domain.radioplayer.RadioPlayerInteractor;
import online.octoapps.radiousa.domain.stations.StationsInteractor;
import online.octoapps.radiousa.presentation.stations.viewmodel.StationViewModel;

public interface StationsContract {

    interface View {

        void addStations(List<StationViewModel> items);

        void clearStations();

        void setHasMoreDataToLoad(boolean b);

        void setHasLoadedAllItems(boolean b);

        void setShowAllMenuItemVisible(boolean b);

        void setShowFavoritesMenuItemVisible(boolean b);

        void setShowAllMenuItemEnabled(boolean b);

        void setShowFavoritesMenuItemEnabled(boolean b);

    }

    interface Presenter {

        void attachStationsInteractor(StationsInteractor interactor);

        void attachRadioPlayerInteractor(RadioPlayerInteractor interactor);

        void onCreate();

        void onDestroy();

        void onLoadMoreStations();

        void onAddToFavoritesButtonClick(long stationId);

        void onRemoveFromFavoritesButtonClick(long stationId);

        void onStationsItemClick(long stationId);

        void onSearchQueryTextChange(String newText);

        void onShowAllMenuItemClick();

        void onShowFavoritesMenuItemClick();

    }

}
