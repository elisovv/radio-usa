package online.octoapps.radiousa.presentation.stations.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import online.octoapps.radiousa.R;
import online.octoapps.radiousa.presentation.stations.viewmodel.StationViewModel;

public class StationsAdapter extends RecyclerView.Adapter<StationsAdapter.ViewHolder> {
    private Context mContext;
    private List<StationViewModel> mItems = new ArrayList<>();
    private Callbacks mCallbacks;

    public interface Callbacks {
        void onItemClick(long stationId);
        void onAddToFavoritesButtonClick(long stationId);
        void onRemoveFromFavoritesButtonClick(long stationId);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_station, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final StationViewModel viewModel = mItems.get(position);
        holder.mStationNameText.setText(viewModel.getName());
        holder.mStationPlaceText.setText(viewModel.getPlace());
        Picasso.with(mContext)
                .load(viewModel.getArtworkUrl())
                .into(holder.mStationArtwork);
        int resId = viewModel.isFavorite()
                ? R.drawable.item_ic_favorite_selected_24dp
                : R.drawable.item_ic_favorite_disabled_24dp;
        holder.mAddToFavoritesButton.setImageResource(resId);
        holder.mAddToFavoritesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewModel.isFavorite()) {
                    viewModel.setFavorite(false);
                    mCallbacks.onRemoveFromFavoritesButtonClick(viewModel.getId());
                } else {
                    viewModel.setFavorite(true);
                    mCallbacks.onAddToFavoritesButtonClick(viewModel.getId());
                }
                notifyItemChanged(holder.getAdapterPosition());
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onItemClick(viewModel.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void addItems(List<StationViewModel> items) {
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mItems.clear();
        notifyDataSetChanged();
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mStationArtwork;
        TextView mStationNameText;
        TextView mStationPlaceText;
        ImageButton mAddToFavoritesButton;

        ViewHolder(View itemView) {
            super(itemView);
            mStationArtwork = (ImageView) itemView.findViewById(R.id.image_station_artwork);
            mStationNameText = (TextView) itemView.findViewById(R.id.text_station_name);
            mStationPlaceText = (TextView) itemView.findViewById(R.id.text_station_place);
            mAddToFavoritesButton = (ImageButton) itemView.findViewById(R.id.button_add_to_favorites);
        }
    }
}
