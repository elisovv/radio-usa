package online.octoapps.radiousa.domain.stations;


public interface StationsInteractor {

    void loadAllStations(int page);

    void loadFavoriteStations(int page);

    void searchStations(String query);

    void addStationToFavorites(long stationId);

    void removeStationFromFavorites(long stationId);

}
