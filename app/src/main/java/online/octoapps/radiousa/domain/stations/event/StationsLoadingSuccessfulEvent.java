package online.octoapps.radiousa.domain.stations.event;

import java.util.List;

import online.octoapps.radiousa.presentation.stations.viewmodel.StationViewModel;

public class StationsLoadingSuccessfulEvent {
    private List<StationViewModel> mStationViewModels;

    public StationsLoadingSuccessfulEvent(List<StationViewModel> stationViewModels) {
        mStationViewModels = stationViewModels;
    }

    public List<StationViewModel> getStationViewModels() {
        return mStationViewModels;
    }
}
