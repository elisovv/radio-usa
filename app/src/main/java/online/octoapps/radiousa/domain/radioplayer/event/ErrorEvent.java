package online.octoapps.radiousa.domain.radioplayer.event;

/**
 * Created by allockye on 13.02.17.
 */

public class ErrorEvent {
    private long mStationId;

    public ErrorEvent(long stationId) {
        mStationId = stationId;
    }

    public long getStationId() {
        return mStationId;
    }
}
