package online.octoapps.radiousa.domain.stations.event;

import java.util.List;

import online.octoapps.radiousa.presentation.stations.viewmodel.StationViewModel;

public class StationsSearchingSuccessfulEvent {
    private List<StationViewModel> mStationViewModels;

    public StationsSearchingSuccessfulEvent(List<StationViewModel> stationViewModels) {
        mStationViewModels = stationViewModels;
    }

    public List<StationViewModel> getStationViewModels() {
        return mStationViewModels;
    }
}
