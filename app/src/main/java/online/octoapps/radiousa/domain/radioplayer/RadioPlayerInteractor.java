package online.octoapps.radiousa.domain.radioplayer;

public interface RadioPlayerInteractor {

    void togglePlayingState(long stationId);

}
