package online.octoapps.radiousa.domain.stations;

import android.os.AsyncTask;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import online.octoapps.radiousa.domain.stations.event.StationsLoadingFailureEvent;
import online.octoapps.radiousa.domain.stations.event.StationsSearchingFailureEvent;
import online.octoapps.radiousa.domain.stations.event.StationsSearchingSuccessfulEvent;
import online.octoapps.radiousa.data.entity.Station;
import online.octoapps.radiousa.data.StationsRepository;
import online.octoapps.radiousa.domain.stations.event.StationsLoadingSuccessfulEvent;
import online.octoapps.radiousa.presentation.stations.viewmodel.StationViewModel;

public class StationsInteractorImpl implements StationsInteractor {
    private StationsRepository mStationsRepository;
    private EventBus mEventBus = EventBus.getDefault();
    private AsyncTask mLastTask = null;

    public StationsInteractorImpl(StationsRepository stationsRepository) {
        mStationsRepository = stationsRepository;
    }

    @Override
    public void loadAllStations(final int page) {
        if (mLastTask != null) {
            mLastTask.cancel(false);
        }

        mLastTask = new AsyncTask<Void, Void, List<Station>>() {
            @Override
            protected List<Station> doInBackground(Void... params) {
                return mStationsRepository.getAll(page);
            }

            @Override
            protected void onPostExecute(List<Station> stations) {
                super.onPostExecute(stations);

                if (stations != null && stations.size() != 0) {
                    List<StationViewModel> viewModels = convertToStationViewModel(stations);
                    mEventBus.post(new StationsLoadingSuccessfulEvent(viewModels));
                } else {
                    mEventBus.post(new StationsLoadingFailureEvent());
                }

                mLastTask = null;
            }
        }.execute();
    }

    @Override
    public void loadFavoriteStations(final int page) {
        if (mLastTask != null) {
            mLastTask.cancel(false);
        }

        mLastTask = new AsyncTask<Void, Void, List<Station>>() {
            @Override
            protected List<Station> doInBackground(Void... params) {
                return mStationsRepository.getFavorites(page);
            }

            @Override
            protected void onPostExecute(List<Station> stations) {
                super.onPostExecute(stations);

                if (stations != null && stations.size() != 0) {
                    List<StationViewModel> viewModels = convertToStationViewModel(stations);
                    mEventBus.post(new StationsLoadingSuccessfulEvent(viewModels));
                } else {
                    mEventBus.post(new StationsLoadingFailureEvent());
                }

                mLastTask = null;
            }
        }.execute();
    }

    @Override
    public void searchStations(final String query) {
        if (mLastTask != null) {
            mLastTask.cancel(false);
        }

        mLastTask = new AsyncTask<Void, Void, List<Station>>() {
            @Override
            protected List<Station> doInBackground(Void... params) {
                return mStationsRepository.search(query);
            }

            @Override
            protected void onPostExecute(List<Station> stations) {
                super.onPostExecute(stations);

                if (stations != null && stations.size() != 0) {
                    List<StationViewModel> viewModels = convertToStationViewModel(stations);
                    mEventBus.post(new StationsSearchingSuccessfulEvent(viewModels));
                } else {
                    mEventBus.post(new StationsSearchingFailureEvent());
                }

                mLastTask = null;
            }
        }.execute();
    }

    @Override
    public void addStationToFavorites(long stationId) {
        if (!mStationsRepository.isFavorite(stationId)) {
            mStationsRepository.addToFavorites(stationId);
        }
    }

    @Override
    public void removeStationFromFavorites(long stationId) {
        if (mStationsRepository.isFavorite(stationId)) {
            mStationsRepository.removeFromFavorites(stationId);
        }
    }

    private List<StationViewModel> convertToStationViewModel(List<Station> stations) {
        List<StationViewModel> viewModels = new ArrayList<>();
        for (Station station: stations) {
            StationViewModel viewModel = new StationViewModel();
            viewModel.setId(station.getId());
            viewModel.setName(station.getName());
            viewModel.setPlace(station.getPlace());
            viewModel.setArtworkUrl(station.getArtworkUrl());
            viewModel.setFavorite(mStationsRepository.isFavorite(station.getId()));
            viewModels.add(viewModel);
        }
        return viewModels;
    }
}
