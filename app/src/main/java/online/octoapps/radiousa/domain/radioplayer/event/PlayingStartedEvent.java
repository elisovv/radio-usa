package online.octoapps.radiousa.domain.radioplayer.event;

public class PlayingStartedEvent {
    private long mStationId;

    public PlayingStartedEvent(long stationId) {
        mStationId = stationId;
    }

    public long getStationId() {
        return mStationId;
    }
}
