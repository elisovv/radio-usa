package online.octoapps.radiousa.domain.radioplayer.event;

public class PlayingStoppedEvent {
    private long mStationId;

    public PlayingStoppedEvent(long stationId) {
        mStationId = stationId;
    }

    public long getStationId() {
        return mStationId;
    }
}
