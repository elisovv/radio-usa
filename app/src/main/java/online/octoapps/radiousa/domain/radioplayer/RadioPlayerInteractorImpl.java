package online.octoapps.radiousa.domain.radioplayer;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import org.greenrobot.eventbus.EventBus;

import online.octoapps.radiousa.data.entity.Station;
import online.octoapps.radiousa.data.entity.Stream;
import online.octoapps.radiousa.data.service.IRadioPlayer;
import online.octoapps.radiousa.data.service.RadioPlayerService;
import online.octoapps.radiousa.data.source.cache.CacheHelper;
import online.octoapps.radiousa.domain.radioplayer.event.ErrorEvent;
import online.octoapps.radiousa.domain.radioplayer.event.LoadingEvent;
import online.octoapps.radiousa.domain.radioplayer.event.PlayingStartedEvent;
import online.octoapps.radiousa.domain.radioplayer.event.PlayingStoppedEvent;

public class RadioPlayerInteractorImpl implements RadioPlayerInteractor, IRadioPlayer.Listener {
    private CacheHelper mCacheHelper;
    private EventBus mEventBus;
    private IRadioPlayer mRadioPlayer;
    private String mBandwidth;

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mRadioPlayer = ((RadioPlayerService.LocalBinder) service).getRadioPlayer();
            mRadioPlayer.registerListener(RadioPlayerInteractorImpl.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mRadioPlayer.unregisterListener(RadioPlayerInteractorImpl.this);
            mRadioPlayer = null;
        }
    };

    public RadioPlayerInteractorImpl(Context context) {
        mCacheHelper = CacheHelper.getInstance();
        mEventBus = EventBus.getDefault();

        Intent intent = new Intent(context, RadioPlayerService.class);
        context.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onPlayingStarted(Station station) {
        mEventBus.post(new PlayingStartedEvent(station.getId()));
    }

    @Override
    public void onPlayingStopped(Station station) {
        mEventBus.post(new PlayingStoppedEvent(station.getId()));
    }

    @Override
    public void onLoading(Station station) {
        mEventBus.post(new LoadingEvent(station.getId()));
    }

    @Override
    public void onError(Station station) {
        mEventBus.post(new ErrorEvent(station.getId()));
    }

    @Override
    public void togglePlayingState(long stationId) {
        if (mRadioPlayer == null) return;

        Station station = mCacheHelper.getStation(stationId);
        String bandwidth = (mBandwidth == null)
                ? station.getStreams().get(0).getBandwidth()
                : mBandwidth;

        if (mRadioPlayer.isPlaying()) {
            Station playingStation = mRadioPlayer.getPlayingStation();
            if (station.getId() == playingStation.getId()) {
                mRadioPlayer.stopPlaying();
            } else {
                mRadioPlayer.startPlaying(station, bandwidth);
            }
        } else {
            mRadioPlayer.startPlaying(station, bandwidth);
        }
    }
}
