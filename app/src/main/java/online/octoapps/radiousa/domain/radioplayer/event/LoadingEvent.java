package online.octoapps.radiousa.domain.radioplayer.event;

public class LoadingEvent {
    private long mStationId;

    public LoadingEvent(long stationId) {
        mStationId = stationId;
    }

    public long getStationId() {
        return mStationId;
    }
}
