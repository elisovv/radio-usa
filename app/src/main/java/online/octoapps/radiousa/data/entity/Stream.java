package online.octoapps.radiousa.data.entity;

import com.google.gson.annotations.SerializedName;

public class Stream {
    @SerializedName("bandwidth")
    private String mBandwidth;
    @SerializedName("url")
    private String mUrl;

    public String getBandwidth() {
        return mBandwidth;
    }

    public void setBandwidth(String bandwidth) {
        mBandwidth = bandwidth;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }
}
