package online.octoapps.radiousa.data.service;

import online.octoapps.radiousa.data.entity.Station;
import online.octoapps.radiousa.data.entity.Stream;

public interface IRadioPlayer {

    enum State {

        IDLE,

        STARTED,

        STOPPED,

        LOADING,

    }

    interface Listener {

        void onPlayingStarted(Station station);

        void onPlayingStopped(Station station);

        void onLoading(Station station);

        void onError(Station station);

    }

    void registerListener(Listener listener);

    void unregisterListener(Listener listener);


    void startPlaying(Station station, String bandwidth);

    void stopPlaying();

    boolean isPlaying();

    Station getPlayingStation();

    Stream getPlayingStream();
}
