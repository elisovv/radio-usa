package online.octoapps.radiousa.data.source.db;

import android.content.ContentValues;
import android.database.Cursor;

import online.octoapps.radiousa.data.entity.Station;
import online.octoapps.radiousa.data.entity.Stream;

class DbScheme {

    abstract static class StationsTable {

        static final String NAME = "stations";

        abstract static class Cols {
            static final String ID = "_id";
            static final String NAME = "name";
            static final String COUNTRY = "country";
            static final String PLACE = "place";
            static final String ARTWORK_URL = "artwork_url";
            static final String LISTENERS = "listeners";
        }

        static final String CREATE =
                "CREATE TABLE " + NAME + " (" +
                        Cols.ID + ", " +
                        Cols.NAME + ", " +
                        Cols.COUNTRY + ", " +
                        Cols.PLACE + ", " +
                        Cols.ARTWORK_URL + ", " +
                        Cols.LISTENERS + ")";

        static ContentValues toContentValues(Station station) {
            ContentValues values = new ContentValues();
            values.put(Cols.ID, station.getId());
            values.put(Cols.NAME, station.getName());
            values.put(Cols.COUNTRY, station.getCountry());
            values.put(Cols.PLACE, station.getPlace());
            values.put(Cols.ARTWORK_URL, station.getArtworkUrl());
            values.put(Cols.LISTENERS, station.getListeners());
            return values;
        }

        static Station parseCursor(Cursor cursor) {
            Station station = new Station();
            station.setId(cursor.getLong(cursor.getColumnIndex(Cols.ID)));
            station.setName(cursor.getString(cursor.getColumnIndex(Cols.NAME)));
            station.setCountry(cursor.getString(cursor.getColumnIndex(Cols.COUNTRY)));
            station.setPlace(cursor.getString(cursor.getColumnIndex(Cols.PLACE)));
            station.setArtworkUrl(cursor.getString(cursor.getColumnIndex(Cols.ARTWORK_URL)));
            station.setListeners(cursor.getInt(cursor.getColumnIndex(Cols.LISTENERS)));
            return station;
        }

    }

    abstract static class StreamsTable {

        static final String NAME = "streams";

        abstract static class Cols {
            static final String ID = "_id";
            static final String STATION_ID = "station_id";
            static final String BANDWIDTH = "bandwidth";
            static final String STREAM_URL = "streamUrl";
        }

        static final String CREATE =
                "CREATE TABLE " + NAME + " (" +
                        Cols.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        Cols.STATION_ID + ", " +
                        Cols.BANDWIDTH + ", " +
                        Cols.STREAM_URL + ")";

        static ContentValues toContentValues(long stationId, Stream stream) {
            ContentValues values = new ContentValues();
            values.put(Cols.STATION_ID, stationId);
            values.put(Cols.BANDWIDTH, stream.getBandwidth());
            values.put(Cols.STREAM_URL, stream.getUrl());
            return values;
        }

        static Stream parseCursor(Cursor cursor) {
            Stream stream = new Stream();
            stream.setBandwidth(cursor.getString(cursor.getColumnIndex(Cols.BANDWIDTH)));
            stream.setUrl(cursor.getString(cursor.getColumnIndex(Cols.STREAM_URL)));
            return stream;
        }

    }

}
