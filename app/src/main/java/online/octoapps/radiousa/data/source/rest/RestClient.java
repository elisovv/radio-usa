package online.octoapps.radiousa.data.source.rest;

import online.octoapps.radiousa.Constants;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static RestClient sInstance = null;

    private StationsApi mStationsApi;

    public static RestClient getInstance() {
        if (sInstance == null) {
            sInstance = new RestClient();
        }
        return sInstance;
    }

    private RestClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mStationsApi = retrofit.create(StationsApi.class);
    }

    public StationsApi getStationsApi() {
        return mStationsApi;
    }
}
