package online.octoapps.radiousa.data;

import android.content.Context;

import java.io.IOException;
import java.util.List;

import online.octoapps.radiousa.Constants;
import online.octoapps.radiousa.data.entity.Station;
import online.octoapps.radiousa.data.source.cache.CacheHelper;
import online.octoapps.radiousa.data.source.db.DbHelper;
import online.octoapps.radiousa.data.source.rest.RestClient;
import online.octoapps.radiousa.util.LogUtil;
import retrofit2.Response;

public class StationsRepositoryImpl implements StationsRepository {
    private DbHelper mDbHelper;
    private CacheHelper mCacheHelper;

    public StationsRepositoryImpl(Context context) {
        mDbHelper = DbHelper.getInstance(context);
        mCacheHelper = CacheHelper.getInstance();
    }

    @Override
    public List<Station> getAll(int page) {
        List<Station> stations = null;
        if (mCacheHelper.containsPage(page)) {
            stations = mCacheHelper.getPage(page);
            LogUtil.i(this, "getAll: from cache");
        } else {
            try {
                Response<List<Station>> response = RestClient.getInstance()
                        .getStationsApi()
                        .getAll(Constants.API_PARAM_COUNTRY, page)
                        .execute();
                stations = response.body();
                mCacheHelper.putStations(stations);
                mCacheHelper.putPage(page, stations);
                LogUtil.i(this, "getAll: " + response.code() + " - " + response.raw().request().url());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return stations;
    }

    @Override
    public List<Station> search(String query) {
        List<Station> stations = null;
        if (mCacheHelper.containsSearchResults(query)) {
            stations = mCacheHelper.getSearchResults(query);
            LogUtil.i(this, "search: from cache");
        } else {
            try {
                Response<List<Station>> response = RestClient.getInstance()
                        .getStationsApi()
                        .search(query, Constants.API_PARAM_COUNTRY)
                        .execute();

                LogUtil.i(this, "search: " + response.code() + " - " + response.raw().request().url());
                stations = response.body();
                mCacheHelper.putStations(stations);
                mCacheHelper.putSearchResults(query, stations);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return stations;
    }

    @Override
    public List<Station> getFavorites(int page) {
        List<Station> stations = mDbHelper.getAll(page);
        mCacheHelper.putStations(stations);
        return stations;
    }

    @Override
    public void addToFavorites(long stationId) {
        Station station = mCacheHelper.getStation(stationId);
        mDbHelper.store(station);
    }

    @Override
    public void removeFromFavorites(long stationId) {
        mDbHelper.delete(stationId);
    }

    @Override
    public boolean isFavorite(long stationId) {
        return mDbHelper.contains(stationId);
    }
}
