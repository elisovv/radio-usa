package online.octoapps.radiousa.data.source.rest;

import java.util.List;

import online.octoapps.radiousa.Constants;
import online.octoapps.radiousa.data.entity.Station;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface StationsApi {
    @GET("stations.php?appId=" + Constants.API_PARAM_APP_ID)
    Call<List<Station>> getAll(@Query("country") String country, @Query("page") int page);

    @GET("search.php?appId=" + Constants.API_PARAM_APP_ID)
    Call<List<Station>> search(@Query("query") String query, @Query("country") String country);
}
