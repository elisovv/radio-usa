package online.octoapps.radiousa.data.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecSelector;
import com.google.android.exoplayer.TrackRenderer;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.upstream.Allocator;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;
import com.google.android.exoplayer.util.Util;

import java.util.ArrayList;
import java.util.List;

import online.octoapps.radiousa.R;
import online.octoapps.radiousa.data.entity.Station;
import online.octoapps.radiousa.data.entity.Stream;

public class RadioPlayerService extends Service implements IRadioPlayer, ExoPlayer.Listener {
    private static final String ACTION_START_PLAYING = "online.octoapps.radiousa.action.ACTION_START_PLAYING";
    private static final String ACTION_STOP_PLAYING = "online.octoapps.radiousa.action.ACTION_STOP_PLAYING";

    private static final int BUFFER_SEGMENT_SIZE = 64 * 1024;
    private static final int BUFFER_SEGMENT_COUNT = 256;

    private IBinder mBinder;
    private List<Listener> mListeners;
    private ExoPlayer mExoPlayer;
    private State mState;
    private Station mStation;
    private Stream mStream;

    public static void start(Context context) {
        Intent intent = new Intent(context, RadioPlayerService.class);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mBinder = new LocalBinder();
        mListeners = new ArrayList<>();
        mExoPlayer = ExoPlayer.Factory.newInstance(1);
        mExoPlayer.addListener(this);
        mExoPlayer.setPlayWhenReady(true);
        mState = State.IDLE;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            if (action != null) {
                switch (action) {
                    case ACTION_START_PLAYING:
                        if (!isPlaying()) startPlaying(mStation, mStream.getBandwidth());
                        break;
                    case ACTION_STOP_PLAYING:
                        if (isPlaying()) stopPlaying();
                        break;
                }
            }
        }
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case ExoPlayer.STATE_BUFFERING:
                mState = State.LOADING;
                notifyLoading();
                break;
            case ExoPlayer.STATE_PREPARING:
                mState = State.LOADING;
                notifyLoading();
                break;
            case ExoPlayer.STATE_READY:
                mState = State.STARTED;
                notifyPlayingStarted();
                break;
            case ExoPlayer.STATE_IDLE:
                mState = State.STOPPED;
                notifyPlayingStopped();
                break;
        }
    }

    @Override
    public void onPlayWhenReadyCommitted() {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        notifyError();
    }

    @Override
    public void registerListener(Listener listener) {
        if (!mListeners.contains(listener)) mListeners.add(listener);
    }

    @Override
    public void unregisterListener(Listener listener) {
        if (mListeners.contains(listener)) mListeners.remove(listener);
    }

    @Override
    public void startPlaying(Station station, String bandwidth) {
        for (Stream stream: station.getStreams()) {
            if (stream.getBandwidth().equals(bandwidth)){
                mStation = station;
                mStream = stream;
                break;
            }
        }

        if(mStation == null || mStream == null) return;

        Uri uri = Uri.parse(mStream.getUrl());
        Allocator allocator = new DefaultAllocator(BUFFER_SEGMENT_SIZE);
        String userAgent = Util.getUserAgent(this, getString(R.string.app_name));
        DataSource dataSource = new DefaultUriDataSource(this, null, userAgent);
        ExtractorSampleSource sampleSource = new ExtractorSampleSource(uri, dataSource, allocator,
                BUFFER_SEGMENT_SIZE * BUFFER_SEGMENT_COUNT);
        TrackRenderer trackRenderer = new MediaCodecAudioTrackRenderer(sampleSource, MediaCodecSelector.DEFAULT);

        mExoPlayer.prepare(trackRenderer);
    }

    @Override
    public void stopPlaying() {
        mExoPlayer.stop();
    }

    @Override
    public boolean isPlaying() {
        return mState == State.STARTED;
    }

    @Override
    public Station getPlayingStation() {
        return mStation;
    }

    @Override
    public Stream getPlayingStream() {
        return mStream;
    }

    private void notifyPlayingStarted() {
        for (Listener listener: mListeners) listener.onPlayingStarted(mStation);
    }

    private void notifyPlayingStopped() {
        for (Listener listener: mListeners) listener.onPlayingStopped(mStation);
    }

    private void notifyLoading() {
        for (Listener listener: mListeners) listener.onLoading(mStation);
    }

    private void notifyError() {
        for (Listener listener: mListeners) listener.onError(mStation);
    }

    public class LocalBinder extends Binder {
        public IRadioPlayer getRadioPlayer() {
            return RadioPlayerService.this;
        }
    }
}
