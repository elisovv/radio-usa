package online.octoapps.radiousa.data.source.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import online.octoapps.radiousa.data.entity.Station;
import online.octoapps.radiousa.data.entity.Stream;

public class DbHelper extends SQLiteOpenHelper {
    private static DbHelper sInstance = null;
    private static final String DATABASE_NAME = "radioUSA.db";
    private static final int DATABASE_VERSION = 2;

    private SQLiteDatabase mDatabase;

    public static DbHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DbHelper(context);
        }
        return sInstance;
    }

    private DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mDatabase = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DbScheme.StationsTable.CREATE);
        db.execSQL(DbScheme.StreamsTable.CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void store(Station station) {
        if (contains(station.getId())) return;
        ContentValues values = DbScheme.StationsTable.toContentValues(station);
        mDatabase.insert(DbScheme.StationsTable.NAME, null, values);
        for (Stream stream: station.getStreams()) {
            values = DbScheme.StreamsTable.toContentValues(station.getId(), stream);
            mDatabase.insert(DbScheme.StreamsTable.NAME, null, values);
        }
    }

    public void delete(long stationId) {
        mDatabase.delete(DbScheme.StationsTable.NAME, DbScheme.StationsTable.Cols.ID + "=" + stationId, null);
        mDatabase.delete(DbScheme.StreamsTable.NAME, DbScheme.StreamsTable.Cols.STATION_ID + "=" + stationId, null);
    }

    public boolean contains(long stationId) {
        return mDatabase.query(DbScheme.StationsTable.NAME, null,
                DbScheme.StationsTable.Cols.ID + "=" + stationId, null, null, null, null)
                .getCount() != 0;
    }

    public List<Station> getAll(int page) {
        int count = 250;
        int offset = count * page;

        List<Station> stations = new ArrayList<>();
        Cursor stationsCursor = mDatabase.rawQuery("SELECT * FROM `" + DbScheme.StationsTable.NAME
                + "` LIMIT " + count + " OFFSET " + offset, null);
        while (stationsCursor.moveToNext()) {
            Station station = DbScheme.StationsTable.parseCursor(stationsCursor);
            List<Stream> streams = new ArrayList<>();
            Cursor streamsCursor = mDatabase.query(DbScheme.StreamsTable.NAME, null, DbScheme.StreamsTable.Cols.STATION_ID + "=" + station.getId(), null, null, null, null);
            while (streamsCursor.moveToNext()) {
                Stream stream = DbScheme.StreamsTable.parseCursor(streamsCursor);
                streams.add(stream);
            }
            streamsCursor.close();
            station.setStreams(streams);
            stations.add(station);
        }
        stationsCursor.close();
        return stations;
    }
}
