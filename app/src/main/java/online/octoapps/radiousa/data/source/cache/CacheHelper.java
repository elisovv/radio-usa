package online.octoapps.radiousa.data.source.cache;

import android.util.SparseArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import online.octoapps.radiousa.data.entity.Station;

public class CacheHelper {
    private static CacheHelper sInstance = null;

    private SparseArray<List<Station>> mPages = new SparseArray<>();
    private HashMap<String, List<Station>> mSearchResultsMap = new HashMap<>();
    private List<Station> mStations = new ArrayList<>();

    public static CacheHelper getInstance() {
        if (sInstance == null) sInstance = new CacheHelper();
        return sInstance;
    }

    private CacheHelper() {
    }

    /**********************************************************************************************/

    public void putPage(int page, List<Station> stations) {
        mPages.put(page, stations);
    }

    public void removePage(int page) {
        mPages.remove(page);
    }

    public void clearPages() {
        mPages.clear();
    }

    public boolean containsPage(int page) {
        return mPages.get(page) != null;
    }

    public List<Station> getPage(int page) {
        return mPages.get(page);
    }

    /**********************************************************************************************/

    public void putSearchResults(String query, List<Station> stations) {
        mSearchResultsMap.put(query, stations);
    }

    public void removeSearchResults(String query) {
        mSearchResultsMap.remove(query);
    }

    public void clearSearchResults() {
        mSearchResultsMap.clear();
    }

    public boolean containsSearchResults(String query) {
        return mSearchResultsMap.containsKey(query);
    }

    public List<Station> getSearchResults(String query) {
        return mSearchResultsMap.get(query);
    }

    /**********************************************************************************************/

    public void putStations(List<Station> stations) {
        for (Station s1: stations) {
            boolean b = false;
            for (Station s2: mStations) {
                if (s1.getId() != s2.getId()) continue;
                b = true;
                break;
            }
            if (!b) mStations.add(s1);
        }
    }

    public void clearStations() {
        mStations.clear();
    }

    public Station getStation(long stationId) {
        for (Station station : mStations) {
            if (station.getId() == stationId) return station;
        }
        return null;
    }
}
