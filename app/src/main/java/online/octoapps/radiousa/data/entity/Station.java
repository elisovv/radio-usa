package online.octoapps.radiousa.data.entity;

import android.text.Html;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Station {
    @SerializedName("id")
    private long mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("artworkUrl")
    private String mArtworkUrl;
    @SerializedName("country")
    private String mCountry;
    @SerializedName("listeners")
    private int mListeners;
    @SerializedName("place")
    private String mPlace;
    @SerializedName("streams")
    private List<Stream> mStreams;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getName() {
        return mName == null ? mName : Html.fromHtml(mName).toString();
    }

    public void setName(String name) {
        mName = name;
    }

    public String getArtworkUrl() {
        return mArtworkUrl;
    }

    public void setArtworkUrl(String artworkUrl) {
        mArtworkUrl = artworkUrl;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public int getListeners() {
        return mListeners;
    }

    public void setListeners(int listeners) {
        mListeners = listeners;
    }

    public String getPlace() {
        return mPlace;
    }

    public void setPlace(String place) {
        mPlace = place;
    }

    public List<Stream> getStreams() {
        return mStreams;
    }

    public void setStreams(List<Stream> streams) {
        mStreams = streams;
    }
}