package online.octoapps.radiousa.data;

import java.util.List;

import online.octoapps.radiousa.data.entity.Station;

public interface StationsRepository {

    List<Station> search(String query);

    List<Station> getAll(int page);

    List<Station> getFavorites(int page);

    void addToFavorites(long stationId);

    void removeFromFavorites(long stationId);

    boolean isFavorite(long stationId);

}
