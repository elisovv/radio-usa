package online.octoapps.radiousa;

import android.app.Application;

import online.octoapps.radiousa.data.service.RadioPlayerService;

public class RadioApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        RadioPlayerService.start(this);
    }
}
