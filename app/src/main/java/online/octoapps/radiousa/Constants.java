package online.octoapps.radiousa;

public class Constants {
    public static final String API_BASE_URL = "https://octoapps.online/radio/api/";
    public static final String API_PARAM_APP_ID = "fbf7a90629e804fac2fe941080725b30";
    public static final String API_PARAM_COUNTRY = "USA";
}
