package online.octoapps.radiousa.util;

import android.util.Log;

public class LogUtil {
    public static void d(Object obj, String msg) {
        Log.d(obj.getClass().getSimpleName(), msg);
    }

    public static void i(Object obj, String msg) {
        Log.i(obj.getClass().getSimpleName(), msg);
    }

    public static void e(Object obj, String msg) {
        Log.e(obj.getClass().getSimpleName(), msg);
    }

    public static void v(Object obj, String msg) {
        Log.v(obj.getClass().getSimpleName(), msg);
    }
}
